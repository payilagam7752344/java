package Hacker_rank;

import java.util.Scanner;

public class flames {

	public static void main(String[] args) {
		flames obj=new flames();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter 2 Names");
		String name1=sc.nextLine().replaceAll(" ","").toUpperCase();
		String name2=sc.nextLine().replaceAll(" ","").toUpperCase();

		System.out.println(obj.flames_game(name1,name2));
		
	}

	private String flames_game(String name1, String name2) {
		String temp1=name1;
		String temp2=name2;
		
		for(int i=0;i<name1.length();i++) 
		{
			for(int j=0;j<name2.length();j++)
			{
				if(name1.charAt(i)==name2.charAt(j))
				{
					name1=name1.replaceFirst(""+name1.charAt(i)," ");
					name2=name2.replaceFirst(""+name2.charAt(i)," ");
					break;
				}
			}
		}
		String org_ip=name1.replaceAll(" ", "")+name2.replaceAll(" ", "");
		
		String flames="FLAMES";
		while(flames.length()!=1)
		{
			int rmv_ch=org_ip.length()%flames.length();
			
			if(rmv_ch!=0)
				flames=flames.substring(rmv_ch)+flames.substring(0,rmv_ch-1);
			else
				flames.substring(0,flames.length()-1);	
		}
		
		System.out.println(flames);
		
		
		return null;
	}

}
