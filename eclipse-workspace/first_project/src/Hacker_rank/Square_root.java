package Hacker_rank;

import java.util.Scanner;

public class Square_root {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Square_root obj = new Square_root();

		int x = sc.nextInt();

		System.out.println(obj.mySqrt(x));
	}

	public static int mySqrt(int x) {
		if (x == 0 || x == 1) {
			return x;
		}

		int start = 1, end = x/2 ;

		while (start <= end) {
			int mid = (start + end) / 2;
			long square = (long) mid * mid; // Using long to avoid integer overflow

			if (square == x) {
				return mid;
			} else if (square < x) {
				start = mid + 1;
			} else {
				end = mid - 1;
			}
		}

		return end; // Return the rounded down square root
	}

}
