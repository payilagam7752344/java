package Hacker_rank;

import java.util.Scanner;

public class Search_insert_position {
//	public static void main(String[] args) {
//		
//		Scanner sc=new Scanner (System.in);
//		int ar=sc.nextInt();
//		
//		int arr[]=new int [ar];
//		for(int a=0;a<arr.length;a++)
//		{
//			 arr[a]=sc.nextInt();
//		}
//		
//		int target=sc.nextInt();
//		
//		
//		for(int i=0;i<arr.length;i++)
//		{
//			if(arr[i]==target)
//			{
//				System.out.println(i);
//			}
//		}
//	}

	public static void main(String [] args)
    {
        Scanner sc= new Scanner(System.in);
        Search_insert_position obj= new Search_insert_position();
        
        int array_size=sc.nextInt();
         int arr[]= new int[array_size];
         
         for(int i=0;i<arr.length;i++)
         {
             arr[i]=sc.nextInt();
         }
         
         int Target=sc.nextInt();
        
        System.out.println(obj.searchInsert(arr,Target));
    }
	
	
	 public static int searchInsert(int[] nums, int target) {
	        int start = 0, end = nums.length - 1;

	        while (start <= end) {
	            int mid = (start + end ) / 2;

	            if (nums[mid] == target) {
	                return mid; // Target found at index mid
	            } 
	            else if (nums[mid] < target) {
	                start = mid + 1; // Adjust the search range to the right half
	            } else {
	                end = mid - 1; // Adjust the search range to the left half
	            }
	        }

	        return start;
	}
	     
	
}
