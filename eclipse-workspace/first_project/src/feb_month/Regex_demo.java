package feb_month;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex_demo {
	public static void main(String[] args) {
		
//		String input ="6382442054";
//		System.out.println(Long.parseLong(input));
//	parse class- use wrapper	
		
		String input2="This is my number 6352455000325";
		
//		Pattern patternObj=Pattern.compile("\\d"); //all numbers taked 

		Pattern patternObj=Pattern.compile("\\d{10}");
		//if you want 10 numbers only - d is meant by regex pattern
        //less then 10 it will not print so u have to introduce boolean.
		
		Matcher matchObj = patternObj.matcher(input2);
		
		boolean condition=false;//extra
		int count=0;//extra
		while(matchObj.find())
		{
//			System.out.println(matchObj.end());
			//numbers last place
//			System.out.println(matchObj.start());
			//	numbers starting position		
			
//			System.out.println(matchObj.group());
			
			count++;
			condition=true;
		}
		
		//if(condition==false  || count>10)
//			System.out.println("Invalid input");
		
// ----------------------------------------------------------------------------------------	
		
//String value
		
	    String ab="This is my number 6352455000325";
		Pattern p_Obj=Pattern.compile("\\D");
		Matcher m_Obj = p_Obj.matcher(ab);
		while(m_Obj.find())
		{
//			System.out.print(m_Obj.group());
		}
		
//Phone number
		String a="This is my number 5382442054";
		Pattern obj=Pattern.compile("(0|91)?[6-9][0-9]{9}");
		Matcher obj2 = obj.matcher(a);
		boolean con=false;
		while(obj2.find())
		{
			System.out.print(obj2.group());
			con=true;
		}
		if(con==false)
			System.out.println("invalid input");
		
	}
}

