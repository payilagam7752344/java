package pattern_programs;

import java.util.Arrays;
import java.util.Scanner;

public class z_pattern {
	
//OUTPUT
//	 z o h o c o r 
//             p   
//           o    
//         r      
//       a        
//     t           
//   i o n t e a m 

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter input count");
		int count=sc.nextInt();
		
		String arr[]=new String [count];
		System.out.println("Enter "+count+" word");
		for(int i=0;i<count;i++) {
			arr[i]=sc.next();		
			}
//		System.out.println(Arrays.toString(arr));
		
		for(int i=0;i<count;i++) {
			String word=arr[i];
			boolean result =false;
			
			for(int grid=1;grid<word.length()/2;grid++) {
				//int grid=1;grid<word.length()  (both (length or length/2)are equal)
				
				int condition_1=word.length()-(grid*2);
				  //7*7 //condition1=19-(7*2) =5
				
				int condition_2=grid-2;
					//condition2=7-2=5
				
				int index=0;
				if(condition_1==condition_2) {
					result=true;
					for(int row=1;row<=grid;row++) {
						
						for(int col=1;col<=grid;col++) {
							if(row==1 || row+col==grid+1 || row==grid) {
								System.out.print(word.charAt(index++)+" ");
							}
							else
								System.out.print("  ");
						}
						
						System.out.println();
					}
					System.out.println();
					System.out.println();
			     }
				
			  }	
			if(result==false)
				 System.out.println("The Given word cannot be print an Z pattern");
	
	}
	
	
}

}
