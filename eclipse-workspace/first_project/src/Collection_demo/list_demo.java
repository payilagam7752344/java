package Collection_demo;

import java.util.ArrayList;
import java.util.Arrays;

public class list_demo {
	
	public static void main(String[] args) {
		
//	Arraylist	
		ArrayList al=new ArrayList();
		al.add(12);
		al.add(12.4);
		al.add("prem");
		al.add(true);
		al.add('c');
		
//	array list - la initialize pandra ellla valuevayum athu object ah than eduthukkum	
//		System.out.println(12);
//		System.out.println(al.contains("prem"));
//		System.out.println(al);
//		
// to find length -- in arraylist we use size() instead of length
		System.out.println(al.size());
		al.add(3,"Thamim");
		System.out.println();
		System.out.println(al);
		
//		al.remove(0);
		System.out.println("remove "+al.remove(0));//int index
//		al.remove("prem"); //object o
		System.out.println("remove "+al.remove("prem"));//int index
		System.out.println(al);
		
		System.out.println(al.remove(al.indexOf(12.4)));

		System.out.println(al);
		
//add all
		ArrayList al1=new ArrayList();
		al1.add(12);
		al1.add(12.4);
		al1.add("prem");
		al1.add(true);
		al1.add('c');
		

		
//	Array 
		int arr[]= {1,2,3,4,5,6};
//		System.out.println(arr);
//		print hashcode only
//		System.out.println(Arrays.toString(arr));
//		System.out.println(arr.length);
		
		}
}
 