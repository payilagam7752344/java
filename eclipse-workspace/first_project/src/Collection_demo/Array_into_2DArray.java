package Collection_demo;

import java.util.ArrayList;
import java.util.List;

public class Array_into_2DArray {
		
  public static void main(String[] args)
  {
	  Array_into_2DArray obj = new Array_into_2DArray();
		int nums[]= {1,3,4,1,2,3,1};
		System.out.println(obj.findMatrix(nums));
	}

	public  List<List<Integer>> findMatrix(int[] nums)
	{
		List<Integer> temp=new ArrayList<>();
		for(int a : nums)
		{
			temp.add(a);
		}
		
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		
		while(temp.size() !=0)
		{
			List<Integer> list1=new ArrayList<Integer>();
			List<Integer> list2=new ArrayList<Integer>();
			
			for(Integer input : temp)
			{
				if(list1.contains(input))
//					contains==> true 
					list2.add(input);
				else
					list1.add(input);
			}
			
			result.add(list1);
			temp=list2;		
		}
		return result;
	}
}
