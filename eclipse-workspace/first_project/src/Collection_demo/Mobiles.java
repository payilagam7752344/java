package Collection_demo;

import java.util.ArrayList;

import java.util.Objects;

//collection's practice problem - 3
public class Mobiles {

	String brand_name;
	int price;

	public Mobiles(String brand_name, int price) {

		this.brand_name = brand_name;
		this.price = price;

	}

	public String toString() {

		return this.brand_name + " = " + this.price;

	}

	public static void main(String[] args) {

		Mobiles m1 = new Mobiles("Vivo", 12000);
		Mobiles m2 = new Mobiles("Oppo", 14000);
		Mobiles m3 = new Mobiles("Apple", 120000);
		Mobiles m4 = new Mobiles("Sony", 18000);
		Mobiles m5 = new Mobiles("One Plus", 21000);

		ArrayList al = new ArrayList();
//		try to use generic - does not need wrapper class
		al.add(m1);
		al.add(m2);
		al.add(m3);
		al.add(m4);
		al.add(m5);

//  System.out.println(m1);
//  System.out.println(Objects.toString(m1));

//  System.out.println(m1.brand_name+" = "+m1.price);

//  System.out.println(al);

		for (Object obj : al) {
			Mobiles mob = (Mobiles) obj;

			if (mob.price < 15000) {
				System.out.println(mob.brand_name + " = " + mob.price);
			}

		}

	}

}