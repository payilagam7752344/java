package Collection_demo;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class hash_map {
	public static void main(String[] args) {
		
		TreeMap hm=new TreeMap();
		HashMap hm2=new  HashMap();
//				key   ,value
		hm.put("sasi", 450);
		hm.put("prem",324);
		hm.put("nisha",439);
		hm.put("divya",440);
		hm.put("nisha",439);
		hm.put("anand",424);
//	key does not accept duplicates , values may accept duplicates	
		System.out.println(hm);
		
//		hm2.putAll(hm);
//		System.out.println(hm2);
//		
//		System.out.println(hm.containsKey("Sasi"));
//		
//		System.out.println(hm.containsValue(440));
//		
//		System.out.println(hm.keySet());
//		System.out.println(hm.values());
		
//Entry
		Set s=hm.entrySet();
		
		int High_Marks =0;
		for(Object obj :s)
		{
			Entry en=(Entry) obj;
			if(High_Marks <= (Integer) en.getValue())
			{
				High_Marks=(Integer)en.getValue();
				System.out.println(en.getKey()+" "+en.getValue());
			}
		}
		
/*Output -
//HashMap - no order -differ on the system
    * {anand=424, sasi=450, divya=440, prem=324, nisha=439}
    * 	anand 424
    * 	sasi 450
		
//LinkedHashMap - what order we give
 * {sasi=450, prem=324, nisha=439, divya=440, anand=424}
 * sasi 450

//TreeMap - alphabetic 
 * {anand=424, divya=440, nisha=439, prem=324, sasi=450}
 * anand 424
 * divya 440
 * sasi 450
		
*/		
		
		
		
		
		
	
	}

}
