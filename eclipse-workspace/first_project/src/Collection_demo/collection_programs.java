package Collection_demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class collection_programs {

	public static void main(String[] args) {
		collection_programs obj=new collection_programs();
		obj.first_prgm();
		obj.second_prgm();
		obj.third_prgm();

	}
	
	//Program - 1 - replace java into python		

	private void first_prgm(){
				String input ="i love java , java is one of the easiest programming languages."
						+ " java has more inbuilt libraries";
				String arr[]=input.split(" ");
				//System.out.println(Arrays.toString(arr));

				ArrayList al = new ArrayList();
				
				for(String word : arr)
				{
					if(word.equals("java"))
						al.add("python");
					else
						al.add(word);
				}
				//System.out.println(al);
				
				String output="";
				for(Object word : al)
				{
					 output=output+word+" ";
				}
				//System.out.println(output);
						
			}
	//program - 2 - How to convert an arrayList to Hashset with and without using constructor.
	
		private void second_prgm() {
			ArrayList marks = new ArrayList();
			
			marks.add(50);
			marks.add(60);
			marks.add(70);
			marks.add(80);
			marks.add(60);
			
//Without constructor 
			//Type- 1
//			HashSet hs = new HashSet();
//			
//			for(Object mk : marks)
//			{
//				hs.add(marks);
//			}
			//Type-2
//			HashSet hs = new HashSet();
//			hs.addAll(marks);
			
//			System.out.println(hs);
			
//With constructor 
			HashSet hs = new HashSet(marks);//to pass argument 
			System.out.println(hs);
			}

		private void third_prgm() {//
			
			//refer mobiles class
			
			
		}


}
