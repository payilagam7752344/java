package Collection_demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Find_Least_Num_Of_Unique_Ints {
	public static void main(String[] args) {
		Find_Least_Num_Of_Unique_Ints solution = new Find_Least_Num_Of_Unique_Ints();
		  int[] nums = { 4, 3, 1, 1, 3, 3, 2 };
		  int k = 3;
		  System.out.println(solution.findLeastNumOfUniqueInts(nums, k)); // Output: 4
		 }
	 public int findLeastNumOfUniqueInts(int[] arr, int k) {
	  HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();

	  for (Integer num : arr) {
	   if (hm.containsKey(num))// true
	    hm.put(num, hm.get(num) + 1);
	   else// false
	    hm.put(num, 1);
	   

	  }

	  ArrayList<Integer> count = new ArrayList<Integer>(hm.values());

	  Collections.sort(count);

	  int Count_size = count.size();
	//  System.out.println(hm);
	//  System.out.println(count);

	  for (Integer ch_count : count) {
	   if (k >= ch_count)// 0>=2
	   {
	    k -= ch_count;
	    Count_size--;
	   }

	  }
	  return Count_size;
	 }

	 
}