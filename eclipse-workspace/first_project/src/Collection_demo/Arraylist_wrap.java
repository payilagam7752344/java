package Collection_demo;

import java.util.ArrayList;
import java.util.Scanner;

public class Arraylist_wrap {

	public static void main(String[] args) {
	
		//to understand how to typecast obj to int

//		ArrayList al=new ArrayList();
//		
//		al.add(60);
//		al.add(70);
//		al.add(80);
//		al.add(90);
//		al.add(100);
//		
//		System.out.println(al); //arrayva print aaguthu
//		
//		int total=0;
//		for(Object num:al)
//		{
//			int temp=(Integer)num;
//			total=total+temp;
////					or
////			total =total+(Integer)num;
//			
//		}
//		System.out.println(total);
		
//to understand how to typecast obj to int //

//TO use scanner class		
		Scanner sc=new Scanner(System.in);
		ArrayList al2=new ArrayList();
	    System.out.println("Enter your marks");  
	    
	    while(true) 
	    {
	      String number=sc.next();
		    if(number.charAt(0)=='N' || number.charAt(0)=='n') 
		    {
		       break;   
		      } 
		     else
		         al2.add(number);
	    }
	    
	    System.out.println(al2);
	    
	    int total_marks=0;
	    for(Object marks : al2)
	    {
	
	//3 line
	    	String number= (String)marks;
	       	//	typecasting
	    	int temp=Integer.parseInt(number);
          	 // string to int
	    	total_marks=total_marks+temp;
	    	
	    	    // or
    //2 line	    	
//	    	int temp2 =Integer.parseInt((String)marks);
//	    	total_marks=total_marks+temp;
	    	
	    		//or
    //1 line	    	
//	    	total_marks =total_marks+Integer.parseInt((String)marks);
	    }
	    System.out.println(total_marks);
		
	}

}
