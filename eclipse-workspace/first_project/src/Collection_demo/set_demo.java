package Collection_demo;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class set_demo {

	public static void main(String[] args) {
		
//Hash_set
//		It does not maintain order and no index based values .
//		if u want no duplicates u can use hash_set
		
		HashSet hs=new HashSet();
		hs.add(10);
		hs.add("Tamim");
		hs.add("Divya");
		hs.add(10);
		hs.add("Nisha");
		hs.add("Dia");
//		System.out.println(hs);
		
//---------------------------------------------------------------------------------------
//		if u want no duplicates with index based value and maintain order..
//		that time u can use linked_hashset

		LinkedHashSet lhs=new LinkedHashSet();	
//		lhs.add(100);
//		lhs.add("Tamil");
//		lhs.add("English");
//		lhs.add(150);
//		lhs.add("Maths");
//		lhs.add("Maths");

//		System.out.println("lhs = "+lhs);
		
//------------------------------------------------------------------------------------------
		String input="Divyapriya";  //o/p -->remove duplicates --> "jav progm"
		String output="";
		
		for(int i=0;i<input.length();i++)
		{
			if(!output.contains(""+input.charAt(i))) //	that means==>	if(output.contains(""+input.charAt(i))==false)
				output=output+input.charAt(i);
		}
		System.out.println(output);
		
//		ithuye linked hashset use pani pannomnaa innnum easy
		
		for(int i=0;i<input.length();i++)
		{
			lhs.add(input.charAt(i));
		}
		System.out.println("lhs = "+lhs);
//output -- [J, a, v,  , p, r, o, g, m]
// if u want jav progm use for enhance loop
		
		for(Object obj:lhs)
		{
			System.out.print(obj);
		}
		
//---------------------------------------------------------------------------------
		
//Treeset
		
	}

}
