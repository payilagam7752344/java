package Collection_demo;

import java.util.ArrayList;

public class Arithmetic_ope_with_wrap_in_arraylist {
	public static void main(String[] args) {
		ArrayList arl=new ArrayList();
		arl.add(true);
		arl.add(133);
		arl.add(155);
		arl.add(false);
		arl.add("Nisha");
		arl.add("Divya");
		arl.add(45.5f);
		arl.add(0);
		arl.add("Thamim");
		arl.add(45);
		arl.add(false);
		arl.add(33.6);
		arl.add(333.6458991000999f);
		arl.add(3.64f);
		arl.add(33.6782335698475);
		arl.add(123565698756324156l);
		arl.add(28);
		arl.add('a');
		arl.add('c');
		


//		System.out.println(arl);
		
	//string only	
		for(Object str : arl)
		{
			try {
			String value=(String)str;
			System.out.println(value);
			}
			catch(ClassCastException cce)
			{
				
			}
		}
		System.out.println();

	//boolean only
		for(Object st : arl)
		{
			try {
			boolean value1=(Boolean)st;
	     	System.out.println(value1);
			
			}
			catch(ClassCastException ce)
			{
				
			}
		}
		
		System.out.println();
		
	//int only
		for(Object int1 : arl)
		{
			try {
				int value2=(Integer)int1;
				System.out.println(value2);
			}
			catch(ClassCastException ce)
			{
				
			}
		}
		
		System.out.println();
	//float only
		for( Object fl: arl)
		{
			try 
			{
				float value3=(Float)fl;
				System.out.println(value3);				
			}
			catch (ClassCastException ce)
			{
				
			}
		}
		
		System.out.println();
		//Double only
			for( Object dl: arl)
			{
				try 
				{
					double value4=(Double)dl;
					System.out.println(value4);				
				}
				catch (ClassCastException ce)
				{
					
				}
			}
			
			System.out.println();
			//long only
				for( Object dl: arl)
				{
					try 
					{
						long value4=(Long)dl;
						System.out.println(value4);				
					}
					catch (ClassCastException ce)
					{
						
					}
				}
				
				System.out.println();
				//byte only
					for( Object dl: arl)
					{
						try 
						{
							byte value4=(Byte)dl;
							System.out.println(value4);				
						}
						catch (ClassCastException ce)
						{
							
						}
					}
					
					System.out.println();
					//Short only
						for( Object dl: arl)
						{
							try 
							{
								short value4=(Short)dl;
								System.out.println(value4);	
							}
							catch (ClassCastException ce)
							{
								
							}
						}
					//char only
							for( Object dl: arl)
							{
								try 
								{
									char value4=(Character)dl;
									System.out.println(value4);				
								}
								catch (ClassCastException ce)
								{
									
								}
							}
		
		
		
		
		
	}
	

}
