package Exception_handling;

import java.util.Scanner;

public class password_validation {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Your Password "
				+ "(The password must be between 8 to 15 characters and must contain at least one lowercase,one uppercase,one special character and one number.)");		String input = sc.next();
		password_validation obj = new password_validation();
		obj.validate_pwd(input);
	}

	private void validate_pwd(String input) {

		int small = 0, spl_ch = 0, number = 0, caps = 0;
		if (input.length() >= 8 && input.length() <= 15) 
		   {
			 
				for (int i = 0; i < input.length(); i++) {
					if (input.charAt(i) >= 'A' && input.charAt(i) <= 'Z')
						caps++;
					else if (input.charAt(i) >= 'a' && input.charAt(i) <= 'z')
						small++;
					else if (input.charAt(i) >= '0' && input.charAt(i) <= '9')
						number++;
					else 
						spl_ch++;
		}
				if (caps == 0) {
					NoCapsException ncp = new NoCapsException();
					throw ncp;
				}
				if (small == 0) {
					NoSmallLetterException nse = new NoSmallLetterException();
					throw nse;
				}
				if (spl_ch == 0) {
					NoSPLcharException nsp = new NoSPLcharException();
					throw nsp;
				}
				if (number == 0) {
					NoNumberException NNE = new NoNumberException();
					throw NNE;
				}
			
			System.out.println("Your Password Created Successfully");

		}

		else {
			PasswordLengthMissmatchException pl = new PasswordLengthMissmatchException();
			throw pl;
		}

	}
}