package muthu_sir_class;

public class Array {
	public static void main(String[] args) {
		
		Array obj=new Array();
		//obj.smallnumber();
		//obj.LinearSearch();
		//obj.LinearSearch_1();
		//obj.LinearSearch_2();
		//obj.LinearSearch();
		//obj.concate_two_array();
		obj.Sub_array();
		//obj.Sub_array_EfficientMethod();
		//obj.Find_First_two_Values();
	}

	private void Sub_array_EfficientMethod() {
		
		boolean present=false;
		
		int a[]= {11,23,45,76,89};
		int b[]= {4,2,7};
		 
		for(int i=0;i<b.length;i++)
		{
			int no=b[i];
			for(int j=0;j<a.length;j++)
			{
				if(a[j]==no)
				{
					present=true;
					break;
				}			
			}
			if(present==false)
			{
				System.out.println("Not sub Array");
				break;
			}
		}
		if(present == true)
			System.out.println("Sub array");
	}

	private void Sub_array() {
		int a[]= {17,33,44,3};
		int b[]= {33,44,23,17}; 
		int count=0;
		
	   for(int i=0;i<b.length;i++)
	   {
		int no=b[i];
		for(int j=0;j<a.length;j++) 
		{
				if(a[j]==no)
				{
					count++;
					break;
				}
		}
	   }
			if(count==b.length)
			{
				System.out.println("Sub array");
			}
			else
				System.out.println("Not sub array");
	}
	

	private void concate_two_array() {
		int a[]= {1,2,3,4};
		int b[]= {10,11,12,13,14};
		int c[]=new int[a.length+b.length];
		for(int i=0;i<c.length;i++)
		{
			if(i<a.length) {
				c[i]=a[i];
				System.out.print(c[i]);
			}
			else
			{
				c[i]=b[i-a.length];
				System.out.print(c[i]);
			}
		}
		
	}

	private void Find_First_two_Values() {
		int bill[]= {230,400,450,300};
		int first=0,second=0;
		for(int i=0;i<bill.length;i++)
		{
			if(bill[i]>first)
			{
				second=first;
				first=bill[i];
			}
			else if(bill[i]>second)
			{
				second=bill[i];
			}
		}
		System.out.println(first);
		System.out.println(second);
	}

	private void LinearSearch_2() {
			String Month[]= {"January","Februry","March","April"};
			int bill[]= {350,450,800,450};
			int count=0;
			
			for(int i=0;i<bill.length;i++)
			{
				if(bill[i]==450)
				{
					System.out.println("Yes Found at "+Month[i]);
					count++;
				}
			}
			System.out.println("count is "+count );
	}

	private void LinearSearch_1() {
			String Month[]= {"January","Februry","March","April","May"};
			int bill[]= {350,500,450,800,450};
			
			for(int i=0;i<bill.length;i++)
			{
				if(bill[i]==450)
				{
					System.out.println("Yes Found at "+Month[i]);
					break;
				}
			}		
	}

	public void LinearSearch() {
		String Month[]= {"January","Februry","March","April"};
		int bill[]= {350,500,800,450};
		
		for(int i=0;i<bill.length;i++)
		{
			if(bill[i]==450)
			{
				System.out.println("Yes Found at "+Month[i]);
			}
		}
	}

	private void smallnumber() {
		int bill[]= {230,340,405,100};
		int small=bill[0];
		
		int i=1;
		while(i<bill.length)
		{
			if(bill[i]<small) 
			{
				small=bill[i];
			}
			i++;
		}
		System.out.println(small);
		
	}
}
