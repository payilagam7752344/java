package muthu_sir_class;

public class Jan_6_Patterns {
  public static void main(String[] args) {
	 
	Jan_6_Patterns obj=new Jan_6_Patterns();
	obj.chritsmas_tree();
	obj.pattern_4thModel();
	obj.pattern_2ndModel();
	obj.pattern_4th_1();
	obj.pattern_4th_2();
	obj.pattern_4th_3();
	obj.pattern_4th_4();//same as 1st pattern model
	obj.pattern_1stModel();
	obj.pattern_odd_even1();
	obj.pattern_odd_even2();
	obj.pattern_odd_even1_1();
	obj.box_starPattern();
	obj.ABC_pattern();
  }

private void ABC_pattern() {
//	A 
//	A B 
//	A B C 
//	A B C D 
//	A B C D E 
	for(char row='A';row<='E';row++)
	{
		for(char col='A';col<=row;col++) 
		{
		   System.out.print(col+" ");
		}
		
		System.out.println();
	}
	System.out.println("----------------");
}

private void box_starPattern() {
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<=5;col++)
		{
			if(row==1 ||row==5 ||col==1 ||col==5)
			   System.out.print("* ");
			else
				System.out.print("  ");
		}
		System.out.println();
	}
	System.out.println("----------------");
}

private void pattern_odd_even1_1() {
//	1 0 1 0 1 
//	1 0 1 0 
//	1 0 1 
//	1 0 
//	1 
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<=6-row;col++)
		{
			if(col%2!=0)
			   System.out.print(1+" ");
			else
				System.out.print(0+" ");
		}
		System.out.println();
	}
	System.out.println("----------------");
}

private void pattern_odd_even2() {
		
//		1 0 1 0 1 
//		1 0 1 0 1 
//		1 0 1 0 1 
//		1 0 1 0 1 
//		1 0 1 0 1 
		for(int row=1;row<=5;row++)
		{
			for(int col=1;col<=5;col++)
			{
				if((row+col)%2==0||row==col)
				{
					System.out.print(1+" ");
				}
				else
					System.out.print(0+" ");
			}
			System.out.println();
		}
		System.out.println("-------------------");

	}	

private void pattern_odd_even1() {
	
//	1 0 1 0 1 
//	0 1 0 1 0 
//	1 0 1 0 1 
//	0 1 0 1 0 
//	1 0 1 0 1 

	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<=5;col++)
		{
			if(col%2==0)
			{
				System.out.print("0"+" ");
			}
			else
				System.out.print("1"+" ");
		}
		System.out.println();
	}
	System.out.println("-----------------------");

}


	
private void pattern_1stModel() {
//		  1 //print star
//	    1 2 
//    1 2 3 
//  1 2 3 4 
//1 2 3 4 5
//	      1 //prtint row
//      2 2 
//    3 3 3 
//  4 4 4 4 
//5 5 5 5 5 
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<6-row;col++) 
		{
		//System.out.print(col+" ");//note
			System.out.print("  ");
		}
		for(int star=1;star<=row;star++) //note
		{
	      System.out.print(star+" ");
		}
		System.out.println();
    }
	System.out.println("-----------");

}

private void pattern_4th_4() {
//		  * 
//	    * * 
//	  * * * 
//  * * * * 
//* * * * *
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<6-row;col++) 
		{
		//System.out.print(col+" ");//note
			System.out.print("  ");
		}
		for(int star=1;star<=row;star++) //note
		{
	       System.out.print("*"+" ");
		}
		System.out.println();
    }
	System.out.println("-----------");

}

private void pattern_4th_3() {
//	1 2 3 4 * * * 
//	1 2 3 * * * 
//	1 2 * * * 
//	1 * * * 
//	* * * 
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<6-row;col++) 
		{
		System.out.print(col+" ");
		}
		for(int star=1;star<=3;star++) 
		{
	       System.out.print("*"+" ");
		}
		System.out.println();
    }
	System.out.println("-----------");
}

private void pattern_4th_2() {
//	1 2 3 4 *
//	1 2 3 *
//	1 2 *
//	1 *
//	*
	
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<6-row;col++) 
		{
		System.out.print(col+" ");
		}
	System.out.println("*");
    }
	System.out.println("-----------");
}


private void pattern_4th_1() {
	
//	1 2 3 4 
//	1 2 3 
//	1 2 
//	1 	
	for(int row=1;row<=4;row++)
	{
		for(int col=1;col<=5-row;col++) 
		{
		System.out.print(col+" ");
		}
	System.out.println();
    }
	System.out.println("-----------");
}

private void pattern_2ndModel() {
//	1 
//	1 2 
//	1 2 3 
//	1 2 3 4 
//	1 2 3 4 5 
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<=row;col++) 
		{
		System.out.print(col+" ");
		}
	System.out.println();

  }
	System.out.println("-----------");

}


private void pattern_4thModel() {
	
//	1 2 3 4 5 
//	1 2 3 4 
//	1 2 3 
//	1 2 
//	1 
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<=6-row;col++) 
		{
		System.out.print(col+" ");
		}
	System.out.println();
  }
	System.out.println("-----------");

}

private void chritsmas_tree() {
	
//        *   
//      *   *   
//    *   *   *   
//  *   *   *   *   
//*   *   *   *   *
  
	for(int row=1;row<=5;row++)
	{
		for(int col=1;col<=5-row;col++) 
		{
		System.out.print("  ");
		}
		for(int star=1;star<=row;star++)
		{
		System.out.print("*"+"   ");
		}
	System.out.println();
    }
	System.out.println("-----------------------");
  }



}

