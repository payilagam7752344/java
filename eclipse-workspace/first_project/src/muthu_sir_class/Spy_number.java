package muthu_sir_class;

import java.util.Scanner;

public class Spy_number {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number");
		int no=sc.nextInt();
		
		int rem=0,sum=0 ,mul=1;
		while(no>0)
		{
			rem=no%10;
			no=no/10;

			sum=sum+rem;
			mul=mul*rem;
		}
		
		if(sum==mul)
			System.out.println("It is a Spy number");
		else
			System.out.println("It is not a Spy number");
	}
	

}
