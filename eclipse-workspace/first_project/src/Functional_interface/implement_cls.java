package Functional_interface;

public class implement_cls implements func_interface{

	@Override
	public void add() {
		System.out.println("hi");
	}
	public static void main(String[] args) {
		//normal implement method 
		implement_cls ic=new implement_cls();
		ic.add();
		
		//lambda method
		func_interface fi= () ->{
			System.out.println("hello");
		};
		fi.add(); 
		
	}

}
