package Scenario_based_questions;

//2) Create a sub class “SQLTrainer” under “Trainer”.
//– Have main method in it.
//– Create instance ram for this class.
//– Handle with proper super class constructor
//– Access parent class instance variables.
//– Call parent class instance method training()
//– Access salary using getter method in parent class

public class SQLTrainer extends Trainer {
	 
	 String word1="";
	 String word2="";

	 public SQLTrainer(String dept, String institute) {
	  super(dept, institute);
	   
	 }
	 public static void main(String[] args) {
	  SQLTrainer Ram=new SQLTrainer("Python", "abced");
	  System.out.println(Ram.dept);
	  System.out.println(Ram.institute);
	  Ram.training();
	  System.out.println(Ram.getSalary());
	 }

	}
