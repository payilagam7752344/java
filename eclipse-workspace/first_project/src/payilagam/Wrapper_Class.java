package payilagam;

public class Wrapper_Class {
	public static void main(String[] args) {
		
		int number=10;
		
		//to convert primitive into non primitive
		//int to Integer
		
		Integer num=Integer.valueOf(number); //boxing
		Integer num2=number; //Auto boxing
		
		Integer age=10;
		
		//Integer into int
		
		int AGE=age.intValue();
		int AGE2=age;
		
		System.out.println(num);	
	}

}
