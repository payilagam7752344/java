package payilagam;

public class reverse_pattern1 {
	

//	* * * * *
//	 * * * *
//	  * * *
//	   * *
//	    *
	
	public static void main(String[] args) {
		int n=9;
		for (int row=0;row<=n;row++) {
			for(int star=n-row;star>0;star--) {
				System.out.print(" ");
			}
			
			for(int space=5;space<row;space++) {
				System.out.print("*"+" ");
			}
			
			System.out.println();

		}
	}

}
