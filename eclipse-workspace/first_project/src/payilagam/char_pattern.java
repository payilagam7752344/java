package payilagam;

public class char_pattern {
	public static void main(String[] args) {
		
//	OUTPUT
//		A
//		A B
//		A B C

//		for(char ch='A';ch<'D';ch++) {
//			for(char ch1='A';ch1<=ch;ch1++) {
//				System.out.print(ch1+" ");
//			}
//			System.out.println();
//		}
		
//2ND METHOD
		
//	OUTPUT
//		A 
//		B C 
//		D E F 
//		G H I J 

//		int n=5;
//		char ch='A';
//		for(int row=0;row<n;row++) {
//			for(int col=0;col<row;col++) {
//				System.out.print(ch+" ");
//				ch++;
//			}
//			System.out.println();
//		}

		
//3RD METHOD

//	OUTPUT
//		A 
//		A B 
//		A B C 
//		A B C D 

		
//		int n=5;
//		for(int row=0;row<n;row++) {
//			char ch='A';
//
//			for(int col=0;col<row;col++) {
//				System.out.print(ch++ +" ");
//				//ch++;
//			}
//			System.out.println();
//		}
		
//TASK -2
		
//   OUTPUT
//		A B C D E 
//		A B C D 
//		A B C 
//		A B 
//		A 

//		int n=5;
//		for(int row=0;row<n;row++) {
//			char ch='A';
//
//			for(int col=4;col>=row;col--) {
//				System.out.print(ch++ +" ");
//			    	}
//			System.out.println();
//		}
		
		
//TASK -3
//		A B C D E F G H G F E D C B A 
//		A B C D E F G   G F E D C B A 
//		A B C D E F       F E D C B A 
//		A B C D E           E D C B A 
//		A B C D               D C B A 
//		A B C                   C B A 
//		A B                       B A 
//		A                           A 

		int n=8;
		for(int row=1;row<=n;row++) {
			char ch='A';
			for(int col=n;col>=row;col--) {
				System.out.print(ch++ +" " );
			}
			for(int space=1;space<row;space++) {
				System.out.print(" "+" "+" "+" ");
			}
			for(int i=n;i>=row;i--) {
				System.out.print(--ch+" ");
			}
			System.out.println();

		}
		
//TASK -4	
		
//		A               A 
//		A B           B A 
//		A B C       C B A 
//		A B C D   D C B A 
//		A B C D E D C B A 
		
//		int n=9;
//		for(int row=1;row<=n;row++){
//			char ch='A';
//				for(int let=1;let<=row;let++) {
//					System.out.print(ch++ +" ");
//				}
//				for(int space=n-1;space>=row;space--){
//					System.out.print(" "+" ");
//				}
//				
//				for(int space2=n-1;space2>=row;space2--) {
//					System.out.print(" "+" ");
//				}
//				for(int let2=1;let2<=row;let2++) {
//					System.out.print(--ch+" ");
//
//				}
//				System.out.println();
//
//			}
		
//TASK - 3
		
//OUTPUT 
		
//		int n=5;
//		for(int row=1;row<=n;row++){
//				for(int let=n;let>=row;let--) {
//					System.out.print("*" +" ");
//				}
//				for(int space=1;space<row;space++){
//					System.out.print(" "+" ");
//				}
//				
//				for(int space2=1;space2<row;space2++) {
//					System.out.print(" "+" ");
//				}
//				for(int let2=n;let2>=row;let2--) {
//					System.out.print("*"+" ");
//
//				}
//				System.out.println();
//
//			}
		
		
//TASK -4
//		
//		int n=5;
//		for(int row=1;row<=n;row++) {
//			for(int col=n-1;col>=row;col++) {
//				System.out.print(col);
//			}
//		}
	
		
	}

}
                                           