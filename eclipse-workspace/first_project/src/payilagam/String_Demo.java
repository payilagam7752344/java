package payilagam;

import java.util.Arrays;
import java.util.Scanner;

public class String_Demo {
		public static void main(String[] args) {
			//string is a class
			//string litrels = without  new keyword -- create obj(direct)
			//string obj create with new keyword ==> string name =new string ("prem") 
			//no arithmetic operator 
			//string is a group of character.it is not datatype.it is a class.
			//string values are immutable(non changeable) 
			//string is a constant.that means it vary but constant 
			//if string values are vary the values are not stored in the memory address(hashcode) it will be create new memory address
		
		//TASK - 1
//			String sasi ="Idly";
//			String prem="Semiya";
//			String Divya ="Dosa";
//			String Nisha="Chappathi";
//			String Tamim ="Idly";
//			
//			System.out.println(sasi.hashCode());
//			System.out.println(prem.hashCode());
//			System.out.println(Divya.hashCode());
//			System.out.println(Nisha.hashCode());
//			System.out.println(Tamim.hashCode());
			
		//TASK - 2 -find index
			
//			String name="prem kumar";
//			System.out.println(name.length());
////			
//			for(int i=0;i<name.length();i++) {
//				System.out.println(name.charAt(i));
//			}

		//TASK -3 - find letter
			
			String name="prem kumar";
			 
//			boolean condition =false;
//			for(int i=0;i<name.length();i++)
//			{
//				if(name.charAt(i)=='k')
//					condition=true;
//			}
//				if(condition==true)
//					System.out.println("k"+" is present");
//				else
//					System.out.println("k"+" not present");
//			
								//or
//				System.out.println(name.contains("z"));
			
		//TASK -4 - STRING PALINDROME
//			
//			Scanner obj =new Scanner(System.in);
//			 
//			System.out.println("Enter any word ");
//			String input=obj.nextLine();
//			
//			String output="";
//			for(int i=input.length()-1;i>=0;i--) {
//				output=output+input.charAt(i);
//				
//			}
//			if(input.equals(output))
//				System.out.println(input+" is a palindrome");
//			else
//				System.out.println(input+" is not a palindrome");
//			
//			//with using method - do palindrome 
//			
//KEYWORDS
			
		//1.Compareto NOTES -STRING INBUILD METHODS
			
			String word_1="divya ";
			String word_2 ="TAMIM ANSARI";
		//	System.out.println(word_1.compareTo(word_2));
		//	System.out.println(word_1.compareToIgnoreCase(word_2));
		//  System.out.println(word_1.concat(word_2));
		//	System.out.println(word_1.contains("v"));
	    // 	System.out.println(word_1.endsWith("vya"));
		//	System.out.println(word_1.startsWith("Di"));
		//	System.out.println(word_1.equals(word_2));
		//	System.out.println(word_1.equalsIgnoreCase(word_2));
		//	System.out.println(word_1.isEmpty());
		//	System.out.println(word_1.isBlank());
		//	System.out.println(word_2.indexOf('a'));
			     	//find first a
		//	System.out.println(word_2.lastIndexOf('a'));
					//find last a
		//	System.out.println(word_2.toCharArray());
			      // String sc="nisha";
			      // char arr[]=new char[sc.length()];
				  // for(int i=0;i<sc.length();i++){
			      //    arr[i]=sc.charAt(i);
			      //   }
				  //  System.out.println(arr);	
			
		//	System.out.println(word_2.toLowerCase());
					
		//	System.out.println(word_1.toUpperCase());
			
		//	System.out.println(Arrays.toString(word_2.split(" ")));
		
			String strip="     Divya     ";
			System.out.println(strip.length());
		//	System.out.println(strip.stripLeading());
			//eliminate front white space
			 	//example
			 			String demo =strip.stripLeading();
						System.out.println(demo.length()+demo);

		//	System.out.println(strip.stripTrailing());	
			//eliminate back white space
				//example
 					String demo1 =strip.stripTrailing();
 					System.out.println(demo1.length()+demo1);

        //  System.out.println(strip.strip());
 			//eliminate white space

            	//example
            		String demo2 =strip.strip();
            		System.out.println(demo2.length()+demo2);

       //   System.out.println(strip.trim());
			//same as strip command 
            
            String word="I               love     tamilnadu";
            System.out.println(word.length());
            
      //    String para=word.replaceAll("\s+"," ");
               //remove unwanted char...\s means remove space...+ symbol system support...in another parameter denoted as what you replace it..like remove unwanted space and update only one space 
//            System.out.println(para);
//            System.out.println(para.length());

            
            
			
			
		}
}
