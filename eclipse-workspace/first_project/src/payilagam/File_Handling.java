package payilagam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class File_Handling {
	public static void main(String[] args) {
		File ff=new File("/home/divyapriya/Desktop/Students/Class/StudentsName/Divya.txt");
		//Create new file
//		try {
//			System.out.println(ff.createNewFile());
//		//	System.out.println(ff.exists());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	//	System.out.println(ff.exists());
	//	ff.delete();
	//	System.out.println(ff.exists());
		
		//create new folder = make directory
		
		//System.out.println(ff.mkdir());
//		System.out.println(ff.mkdirs());
		
		//Write into text file - we can rewrite 
//		try {
//			FileWriter Writer=new FileWriter(ff);
//			Writer.write("Divya");
//			Writer.close();	 //mandatory
//		}
//		catch (IOException e) {
//			e.printStackTrace();
//		}
		
		//Write into text file - we can uppend another word like prem, thamim 

		
//		try {
//			FileWriter Writer=new FileWriter(ff,true); //true is append
//			Writer.write("Divya");
//			Writer.close();	 //mandatory
//		}
//		catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		try {
//			FileWriter Writer=new FileWriter(ff,true); //true is append
//			Writer.write(" \n Rahul");
//			Writer.close();	 //mandatory
//		}
//		catch (IOException e) {
//			e.printStackTrace();
//		}
	
		
	//BufferWriter
		
//		try {
//			FileWriter Writer=new FileWriter(ff,true); //true is append
//			BufferedWriter bw= new BufferedWriter(Writer);
//			bw.write("Bh");
//			bw.newLine();
//			bw.write("Kavin");
//			bw.write("\n Kavi");
//			bw.close();
//			}
//		catch (IOException e) {
//			e.printStackTrace();
//		}
		
//----------------------------------------------------------------------------------------------------------------------------------------------
		
 	//file reader
		
//		try {
//			FileReader reader=new FileReader(ff);
//			//ff.canRead(); //for locked files
//			//ff.canWrite();  //for locked files	
//			
//			int count=0;
//			int word=reader.read();
//			
//			while(word!= -1) //-1 have no ascii value
//			{
//				//System.out.print(word);//int value returns
//				//System.out.print((char)word );
//				count++;
//				word=reader.read();
//			}
//			System.out.println();
//			System.out.println(count);
//		} 
//		catch (FileNotFoundException e) {
//			e.printStackTrace();
//
//		}
//		catch (IOException e) 
//		{
//			e.printStackTrace();
//		}
//		
		//count == space and letters
		
//		try {
//			FileReader reader=new FileReader(ff);
//			
//			int count=0;
//			int word=reader.read();
//			int space=0;
//			
//			while(word!= -1) //-1 have no ascii value
//			{
//				if((char) word==' ')
//					space++;
//				else 	
//					count++;
//				
//				word=reader.read();
//			}
//			System.out.println("Char count is "+count + " Space count is "+space);
//		} 
//		catch (FileNotFoundException e) {
//			e.printStackTrace();
//
//		}
//		catch (IOException e) 
//		{
//			e.printStackTrace();
//		}

		//BufferFile reader
//		try {
//			FileReader reader=new FileReader(ff);
//			BufferedReader br=new BufferedReader(reader);
//			String word=br.readLine();
//			
//			int line_count=0;
//			while(word!= null) //-1 have no ascii value
//			{
//				System.out.println(word);
//				line_count++;
//				word=br.readLine();
//			}
//			System.out.println( line_count); //reduce the code 
//		} 
//		catch (FileNotFoundException e) {
//			e.printStackTrace();
//
//		}
//		catch (IOException e) 
//		{
//			e.printStackTrace();
//		}
		
		//real time scenario
		
		String list[]=ff.list();
		for(int i=0;i<list.length;i++)
			 {
			  
			  if(list[i].startsWith("S"))
			  System.out.println(list[i]);
			 }

	
		
}
}
