package payilagam;

public class pattern1 {
	
//OUTPUT
//	    * 
//	   * * 
//	  * * * 
//	 * * * * 
//	* * * * * 

	public static void main(String[] args) {
		int n=5;
		for (int row=0;row<=n;row++) {
			for(int space=n-row;space>0;space--) {
				System.out.print(" ");
			}
			for(int star=0;star<row;star++) {
				System.out.print("*"+" ");
			}
			System.out.println();

		}
	}

}
