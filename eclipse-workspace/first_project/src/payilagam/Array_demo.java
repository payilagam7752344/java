package payilagam;

import java.util.Arrays;
import java.util.Scanner;

public class Array_demo {

	public static void main(String[] args) {
		int marks[]= {10,50,60,73,100,-150};
		
//find length	
//		System.out.println(marks.length);

//find index 
//		System.out.println(marks[0]);
//		System.out.println(marks[1]);
//		System.out.println(marks[2]);
//		System.out.println(marks[3]);
//		System.out.println(marks[4]);

//looping 
		
//		for(int i=0;i<marks.length;i++) {
//			System.out.print(marks[i]+" ");
//			System.out.println();
		
//		System.out.println(Arrays.toString(marks));
//		}
	
//add 		
//		int total=0;
//		for(int j=0;j<marks.length;j++) {
//			total=total+marks[j];
//			}
//		System.out.println("TOTAL MARKS = " + total);

//find odd or even
		
//		if(total%2==0)
//			System.out.print(total);
		
//find 100 value is there or not in array
		
//		int finding_number=50;
//		boolean condition=false;
		
//		for(int k=0;k<marks.length;k++) 
//		{
//			if(marks[k]==finding_number)
//			{
//				condition=true;
//				break;
//			}
//		}
//		if(condition==true)
//			System.out.println("Yes present");
//		else
//			System.out.println("Not present");
		
//find biggest number
	
//		//int big=0;
//		//0 is neutral //suppose minus value vantha interger.min value nu kuduthukkalam
//		int big=Integer.MIN_VALUE;
//		for(int i=0;i<marks.length;i++) {
//			if(big<marks[i]) 
//				big=marks[i];	
//		}
//		System.out.println("Big number is "+big);

//find small number
		
//	    int small=marks[0];
//		//int small=Integer.MAX_VALUE;
//		for(int s=0;s<marks.length;s++) {
//			if(small>marks[s])
//				small=marks[s];
//		}
//		System.out.println(small);
	
//TYPE 2 
//calculate marks -- getting input from user
		
//		Scanner obj=new Scanner(System.in);
		
//		System.out.println("Enter Array Size ");
//		int size =obj.nextInt();
		
//		int marks1[]=new int[size];
		
//		System.out.println("Enter "+size+" values");
//		int total=0;
		//getting input from user
	
//		for(int i=0;i<size;i++)
//		{
//			marks1[i]=obj.nextInt();
//			//total+=marks[i]
//			total=total+marks1[i];
//		}
//		System.out.println(total);
		
//TYPE 3 
		
//		Scanner obj=new Scanner(System.in);
		
//		System.out.println("Enter Array Size ");
//		int N =obj.nextInt();
		
//		int A[]=new int[N];
		
//		System.out.println("Enter "+N+" Values ");
//		for(int i=0;i<N;i++)
//		{
//		    A[i]=obj.nextInt();
//		}
		
//		System.out.println("Enter x value ");
//		int x=obj.nextInt();
		
//		System.out.println("Enter y value ");
//		int y=obj.nextInt();
		
//without scanner 	
		
//		//int a[]= {4,3,7,11,50,1};
//		//int x=5,y=10;
		
//		for(int i=0;i<A.length;i++)
//		{
//			if(x<y && A[i]>x && A[i]<y) 
//			{
//				System.out.print(A[i]+" ");
//			}
//		}

//TWO DIMENSION ARRAY	
		
		Scanner obj=new  Scanner(System.in);
		
		System.out.println("Enter Row Size");
		int row_size=obj.nextInt();
		
		System.out.println("Enter Col size");
		int col_size=obj.nextInt();
		
		int arr[][]=new int [row_size][col_size];
		System.out.println("Enter "+row_size*col_size+" Values");
		
		for(int row=0;row<row_size;row++)
		{
			for(int col=0;col<col_size;col++) 
			{
				arr[row][col]=obj.nextInt();				
		    }
		}
		
//printing array values	
		
		for(int row=0;row<row_size;row++) 
		{
			for(int col=0;col<col_size;col++)
			{
				System.out.print(arr[row][col]+" ");
		     }
			System.out.println();
		}
		System.out.println();


//total col values 
		
//		for(int row=0;row<row_size;row++) {
//			int total=0;
//			for(int col=0;col<col_size;col++) {
//				total+=arr[row][col];
//		   }
//			System.out.print(total);
//
//			System.out.println();
//		}
		
//add row values		
	
//		for(int row=0;row<row_size;row++) {
//			int total=0;
//			for(int col=0;col<col_size;col++) {
//				total+=arr[col][row];//note
//		   }
//			System.out.print(total);
//
//			System.out.println();
//		}
		
//cross add		
		
//		int total=0;
//		int total1=0;
//		for(int row=0;row<row_size;row++) 
//		{
//			for(int col=0;col<col_size;col++)
//			{
//				if(row==col) 
//			    	total=total+arr[row][col];
//				if(row+col==col_size-1)
//					total1=total1+arr[row][col];		
//		     }
//		}
//		System.out.println(total);
//	    System.out.println(total1);
 }
		
}

