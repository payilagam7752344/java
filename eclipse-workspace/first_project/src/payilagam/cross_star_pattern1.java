package payilagam;


	import java.util.Scanner;

	public class cross_star_pattern1 {
		public static void main(String[] args) {
			cross_star_pattern1 obj = new cross_star_pattern1 ();
			cross_star_pattern1 obj1 = new cross_star_pattern1 ();

			
	//with use scanner 
			
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter any number");
			int no=sc.nextInt();
			
	//assign a value in program
			// int no=5;
			
		//	obj.methodcall(no);
			
	//suppose we want only odd number

			
			if(no%2!=0) {
				obj.methodcall(no);
			}
			else {
				System.out.println("Enter only odd number :");
			}
			
		}

		public void methodcall(int input) {
			for(int row=0;row<input;row++) {
				for(int col=0;col<input;col++) {
					if(row==col || row+col==input-1)
						System.out.print("*"+" ");
					else
						System.out.print("  ");
				}
				System.out.println();
			}
			
		}
				
}
