package Postgres_Sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Learn_Jdbc
{
  public static void main(String[] args) throws SQLException
  {
		
		Learn_Jdbc jd=new Learn_Jdbc();
//		jd.select();
//		jd.create();
//		jd.update();
		jd.delete();	
  }

	private void delete() throws SQLException 
	{
		String url="jdbc:postgresql://localhost:5432/ems";
		String user_name="postgres";
		String password="divya";
		String query="delete from employees where employee_id=?";
		Connection con=DriverManager.getConnection(url, user_name, password);
		
		PreparedStatement pst=con.prepareStatement(query);
		
		pst.setInt(1,10);
		
		int i=pst.executeUpdate();
		System.out.println(i);
//		it returns boolean value like 1 or 0
    }

	private void update() throws SQLException {
		String url="jdbc:postgresql://localhost:5432/ems";
		String user_name="postgres";
		String password="divya";
		String query="update employees set employee_name=? ,department_id=? where employee_id=?";
		Connection con=DriverManager.getConnection(url, user_name, password);
		
		PreparedStatement pst=con.prepareStatement(query);
		
		pst.setString(1,"hiii");
		pst.setInt(2,4);
		pst.setInt(3,6);
			
		int i=pst.executeUpdate();
		System.out.println(i);
//		it returns boolean value like 1 or 0
    }

	private void create() throws SQLException
	{
		String url="jdbc:postgresql://localhost:5432/ems";
		String user_name="postgres";
		String password="divya";
		String query="insert into employees values(?,?,?)";
		Connection con=DriverManager.getConnection(url, user_name, password);
		
		PreparedStatement pst=con.prepareStatement(query);
		
		pst.setInt(1,10);
		pst.setString(2,"cd");
		pst.setInt(3,3);
	
//		pst.executeUpdate(); //exception throw pannum, so
		
		int i=pst.executeUpdate();
		System.out.println(i);
//		it returns boolean value like 1 or 0
    }

	private void select() throws SQLException
	{
		String url="jdbc:postgresql://localhost:5432/ems";
		String user_name="postgres";
		String password="divya";
		
		//connection,statement,resultset is a sql class
		Connection con=DriverManager.getConnection(url, user_name, password);
		Statement st=con.createStatement();
		ResultSet rs= st.executeQuery("select * from employees");

// select query
		while (rs.next())
		{
//			System.out.println(rs.getInt(1));
//						or
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getInt(3));
		} 		
	 }


}
