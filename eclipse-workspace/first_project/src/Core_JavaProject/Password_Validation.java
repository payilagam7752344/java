package Core_JavaProject;

import java.util.Scanner;

public class Password_Validation {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
	    System.out.println("Enter Your Password "
				+ "(The password must be between 8 to 15 characters and must contain at least one lowercase,one uppercase,one special character and one number.)");
				String Password=sc.next();
				int Caps_count=0;
				int Small_count=0;
				int Nums_count=0;

				int Spl_count=0;

		if(Password.length()>=8 &&Password.length()<=15)
		{
			for(int i=0;i<Password.length();i++)
			{
				if(Password.charAt(i)>='A'&& Password.charAt(i)<='Z')
				{
					Caps_count++;
				}
				else if(Password.charAt(i)>='a'&& Password.charAt(i)<='z')
				{
					Small_count++;
				}
				else if(Password.charAt(i)>='0'&& Password.charAt(i)<='9')
				{
					Nums_count++;
				}
				else	
				{
					Spl_count++;
				}
			}
			if(Caps_count==0) 
				System.out.println("Invalid Password! Your password must contain atleast one Capital letter.");
			if(Small_count==0) 
				System.out.println("Invalid Password! Your password must contain atleast one Small letter.");
			if(Nums_count==0) 
				System.out.println("Invalid Password! Your password must contain atleast one Number.");
			if(Spl_count==0) 
				System.out.println("Invalid Password! Your password must contain atleast one Special Character.");
			
			System.out.println("Your Password Created Successfully!");
		}
		else
			System.out.println("Password length should allowed 8-15 characters only");
	}

}
