package Leetcode;

import java.util.Arrays;
import java.util.Scanner;

public class Two_sum {
	
	public static void main(String[] args)
	{
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter array size");
			int arr_size =sc.nextInt();	
			
			int arr[]=new int [arr_size];
			
			System.out.println("Enter "+arr_size+" array values");
			for(int a=0;a<arr.length;a++)
			{
				arr[a]=sc.nextInt();
			}
			
			System.out.println("Enter target value");
			int target=sc.nextInt();
			
			Two_sum obj=new Two_sum();
//			int result[]=obj.twoSum(arr,target);
			System.out.println(Arrays.toString(obj.twoSum(arr,target)));
		}

		public int[] twoSum(int arr [], int target) 
		{
			
			for(int value1=0;value1<arr.length;value1++)
			 {
				for(int value2=arr.length-1;value2>value1;value2--)
				{
					int sum=arr[value1]+arr[value2];
					   if(sum==target)
					   {
						return new int[] {value1,value2};
					   }
					
				}

			}
			return new int[] {};
		}
		
		
		//type-2
		
		/* for(int i=0;i<nums.length;i++)
		 {
	         for(int j=i+1;j<nums.length;j++)
	         {
	             if(nums[j]==target-nums[i])
	             {
	                 return new int[]{i,j};
	             }
	         }
	     }
		 return new int[]{};    	 
		*/
		
	
		

}
