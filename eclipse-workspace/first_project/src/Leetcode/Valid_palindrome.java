package Leetcode;

public class Valid_palindrome {
	 
	    public boolean isPalindrome(String s) {
		
		s=  s.toLowerCase();
	
		String output="";
		String rev="";
		for(int i=0;i<s.length();i++)
		{
			if(s.charAt(i)>='a'&&s.charAt(i)<='z'||s.charAt(i)>='0'&&s.charAt(i)<='9')
				output=output+s.charAt(i);		
		}
		for(int j=output.length()-1;j>=0;j--)
		{
			rev=rev+output.charAt(j);
		}
	
	if(output.equals(rev))
		return true;
	else
		return false;
	
	}
	    public static void main(String[] args) {
	    	Valid_palindrome obj=new Valid_palindrome();
			System.out.println(obj.isPalindrome("madam"));
		}
}