package Leetcode;

import java.util.Arrays;

public class Anagram {

	public boolean isAnagram(String s, String t) {
		    
	  if(s.length()==t.length())	   
		 { 	
		    char ch[]=	s.toLowerCase().toCharArray();
		    char ch2[]=t.toLowerCase().toCharArray();	    
		 
		    Arrays.sort(ch);
		    Arrays.sort(ch2);

		    if(Arrays.equals(ch,ch2))
		    	return true;
		    else
				return false;
	      }
	  
	  else
		return false;
		    }		    
public static void main(String[] args) {
	Anagram obj=new Anagram();
	System.out.println(obj.isAnagram("Anagram","nagaram"));
		
  }

}
