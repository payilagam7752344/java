package Leetcode;

public class Find_firstPalindrome {

	public static void main(String[] args) {
		String words[] = { "abc", "car", "ada", "racecar", "cool", "amma" };
		Find_firstPalindrome obj = new Find_firstPalindrome();
//		String result = obj.first_Palindrome(words);
			//My method
		System.out.println(obj.firstPalindrome(words));
			//Binary search

		//boolean
//		for (int i = 0; i < words.length; i++) {
//			
//			String value = words[i];
////			boolean isPalindrome=obj.checkPalindrome(value);
//			if(isPalindrome) {
//				System.out.println(value);
//
//				break;
//			}
//		}
		
	}

	public String firstPalindrome(String[] words) {
		for (int i = 0; i < words.length; i++) {

			String value = words[i];

			int start = 0;
			int end = value.length() - 1;
			while (start < end) {
				if (value.charAt(start) != value.charAt(end)) {
					break;
				}
				start++;
				end--;
			}
			if (end <= start) {
//			System.out.println(value+" is palindrome");
				return value;
			}
		}
		return "";
	}

	public boolean checkPalindrome(String value) {
		int start = 0;
		int end = value.length() - 1;
		while (start < end) {
			if (value.charAt(start) != value.charAt(end)) {
				break;
			}
			start++;
			end--;
		}
		if (end <= start) {
//			System.out.println(value+" is palindrome");
			return true;
		}
		return false;
	}

	public String first_Palindrome(String[] words) {

		for (int i = 0; i < words.length; i++) {
			String value = words[i];
			String temp = "";

			for (int j = value.length() - 1; j >= 0; j--) {
				temp = temp + value.charAt(j);
			}
			if (temp.equalsIgnoreCase(value)) {
				return value;
			}
		}

		return null;

	}

}
