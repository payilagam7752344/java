package Leetcode;

public class Remove_Duplicates_from_Sorted_Array{	
	public int removeDuplicates(int[] nums) {

        int uniqueCount = 1;
    	
    	for(int i=0; i<nums.length-1;i++)
    	{
    		if(nums[i] != nums[i+1]) {
    			
    			nums[uniqueCount] = nums[i+1];
    			uniqueCount++;	
    		}
    	}
		return uniqueCount;
    }
	public static void main(String[] args) {
		int nums[]= {1,1,2,2,3,3,3,3,5};
		
		//Solution obj=new Solution();
	}
}




