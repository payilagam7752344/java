package uae;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Solution ob = new Solution();
 
        String s= "II";
        System.out.println("Integer form of Roman Numeral"
                           + " is "+ ob.romanToint(s));  
    }
    int value(char r)
    {
        if (r == 'I')
            return 1;
        if (r == 'V')
            return 5;
        if (r == 'X')
            return 10;
        if (r == 'L')
            return 50;
        if (r == 'C')
            return 100;
        if (r == 'D')
            return 500;
        if (r == 'M')
            return 1000;
        return -1;
    }
    int romanToint(String s)
    {
        int temp = 0;
 
        for (int i = 0; i < s.length(); i++) {
            int a = value(s.charAt(i));
            if (i + 1 < s.length()) //0+1<3
            {
                int b = value(s.charAt(i + 1));
                 if (a >= b) {
                     temp = temp +a;
                   }
                else {
                    temp = temp+b-a;
                    i++;
                    }
            }
            else 
            {
                temp = temp+a;
            }
        }
        return temp;
    } 
}