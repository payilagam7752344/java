package uae;

public class roman_int {

	int value(char r)
    {
        if (r == 'I')
            return 1;
        if (r == 'V')
            return 5;
        if (r == 'X')
            return 10;
        if (r == 'L')
            return 50;
        if (r == 'C')
            return 100;
        if (r == 'D')
            return 500;
        if (r == 'M')
            return 1000;
        return -1;
    }
    int romanToint(String str)
    {
       int temp = 0;
 
        for (int i = 0; i < str.length(); i++)
        {
          int a = value(str.charAt(i));
            if (i+1 < str.length()) //0+1<3
            {
               int b = value(str.charAt(i + 1));
                 if (a >= b)
                  {
                	 temp = temp +a;
                  }
                else {
                	temp = temp+b-a;
                    i++;
                    }
            }
            else 
            {
            	temp = temp+a;
            }
        }
        return temp;
    }
    public static void main(String args[])
    {
    	roman_int ob = new roman_int();
 
        String str = "XXXXIX";
        System.out.println("Integer form of Roman Numeral"
                           + " is "
                           + ob.romanToint(str));
    }
}
