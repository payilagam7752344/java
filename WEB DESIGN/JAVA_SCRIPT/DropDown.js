let button=document.querySelector('#btn')
let result=document.createElement('h3')
result.id="output"
document.getElementById('parent').appendChild(result)

button.addEventListener('click',display)

function display(){
    let input =document.querySelector('#input')
    let city = input.options[input.selectedIndex].value

    console.log(city)

    let population =0,language =' ', growth_rate=0;
    switch(city)
    {
        case 'Bangalore' : population=13600800
                            language="Kannada"
                            growth_rate=3.15
                            break;
                            
        case 'Chennai' : population=209110800
                            language="Tamil"
                            growth_rate=2.37
                            break;

        case 'Mumbai' : population=100405060
                            language="Hindi"
                            growth_rate=1.6
                            break;

    }

let text =`The current population of ${city} is ${population} as of today, based on worldometer 
elaboration of the latest united Nations data. 
Growing rate is ${growth_rate} People who spoken language is ${language}.`

// document.querySelector('h3').innerHTML=text
        // or
result.innerHTML=text


}
